﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JacksonFrankland.Engine.Variables;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour
{
	public IntReference score;
	public IntReference highScore;
	public IntReference previousScore;
	public Text rightScore;
	public Vector2Reference playerPosition;

	private Text text;

	void Start ()
	{
		text = GetComponent<Text>();
		score.Value = 0;
	}

	void Update ()
	{
		int oldScore = score.Value;
		score.Value = (int)((playerPosition.Value.y + 3.34) / 12.4f);
		if(score.Value > oldScore) {
			GetComponent<AudioSource>().Play();			
		}
		text.text = "Score: " + score.Value;
		rightScore.text = "High Score: " + highScore.Value + "\nPrevious Score: " + previousScore.Value;
	}
}
