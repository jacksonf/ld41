using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JacksonFrankland.Engine.Variables;

[RequireComponent(typeof(UpdateVelocity))]
public class TopDownController : MonoBehaviour
{
	public Vector2Variable outputPositionTo;

    private UpdateVelocity updateVelocity;

    void Start ()
    {
        updateVelocity = GetComponent<UpdateVelocity>();
    }

    void Update ()
	{
        Vector2 direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
		updateVelocity.direction = direction;
        if(outputPositionTo) {
		    outputPositionTo.SetValue(transform.position);
        }
	}

}