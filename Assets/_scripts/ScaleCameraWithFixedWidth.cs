﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JacksonFrankland.Engine.Variables;

public class ScaleCameraWithFixedWidth : MonoBehaviour {

	public FloatReference width;

	private Camera camera;

	void Start ()
	{
		camera = GetComponent<Camera>();
	}

	void Update ()
	{
		camera.orthographicSize = width / camera.aspect;
	}
}
