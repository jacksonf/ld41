﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointAtMouse : MonoBehaviour {

	void Update () {
		Vector3 mousePostion = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Vector3 perpendicular = Vector3.Cross(transform.position-mousePostion, Vector3.forward);
		transform.rotation = Quaternion.LookRotation(Vector3.forward, perpendicular);
	}
}
