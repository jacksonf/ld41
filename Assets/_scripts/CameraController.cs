﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JacksonFrankland.Engine.Variables;

public class CameraController : MonoBehaviour {

	public Vector2Variable followPosition;
	public FloatReference speed;
	public FloatReference speedSmoothTime;

	private float velocity = 0;
	private float lowBoundary;

	void Start ()
	{
		lowBoundary = transform.position.y;
	}
	
	void Update ()
	{
		float newPosition = Mathf.SmoothDamp(
			transform.position.y, followPosition.Value.y, ref velocity, speedSmoothTime, Mathf.Infinity, Time.deltaTime
		);
		newPosition = Mathf.Clamp(newPosition, lowBoundary, Mathf.Infinity);
		transform.transform.position = new Vector3(
			transform.position.x,
			newPosition,
			transform.position.z
		);
	}
}
