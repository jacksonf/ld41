﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JacksonFrankland.Engine.Variables;

[RequireComponent(typeof(Rigidbody2D))]
public class Car : MonoBehaviour {

	public FloatReference minSpeed;
	public FloatReference maxSpeed;
	public BoolReference goingRight;

	void Start () {
		GetComponent<Rigidbody2D>().velocity = Vector2.right * Random.Range(minSpeed, maxSpeed) * (goingRight ? 1 : -1);
		if(!goingRight) {
			transform.localScale = new Vector3(
				transform.localScale.x * -1, transform.localScale.y, transform.localScale.z
			);
		}
	}
}
