﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JacksonFrankland.Engine.Variables;

public class CarGenerator : MonoBehaviour {

	public GameObject carPrefab;
	public FloatReference minIntervalTime;
	public FloatReference maxIntervalTime;
	public BoolReference goingRight;

	private float intervalTime;
	private float lastSpawnTime;

	void Start ()
	{
		lastSpawnTime = Time.time;
		intervalTime = 0;
	}
	
	void Update ()
	{
		if(Time.time - lastSpawnTime > intervalTime) {
			GameObject car = Instantiate(carPrefab);
			car.transform.position = transform.position;
			car.transform.SetParent(transform);
			car.GetComponent<Car>().goingRight = goingRight;
			lastSpawnTime = Time.time;
			intervalTime = Random.Range(minIntervalTime, maxIntervalTime);
		}
	}
}
