﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fly : MonoBehaviour 
{

	private static List<GameObject> flys = new List<GameObject>();

	void Start ()
	{
		flys.Add(gameObject);
	}

	public static void DestroyFlys ()
	{
		flys.ForEach((fly) => {
			Destroy(fly);
		});
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if(other.transform.GetComponent("Bullet") != null) {
			GetComponent<AudioSource>().Play();
			transform.position = Vector3.back * 1000;
			Destroy(other.gameObject);
			Destroy(gameObject, 5f);
		}
	}

}
