﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JacksonFrankland.Engine.Variables;

public class PointAt : MonoBehaviour {

	public Vector2Reference pointPoint;

	private Rigidbody2D rigidbody2d;

	void Start ()
	{
		rigidbody2d = GetComponent<Rigidbody2D>();
	}

	void Update ()
	{
		Vector2 followPoint = rigidbody2d.velocity + (Vector2)transform.position;
		Vector3 pointPosition = new Vector3(followPoint.x, followPoint.y, transform.position.z);
		Vector3 perpendicular = Vector3.Cross(transform.position-pointPosition, Vector3.forward);
		transform.rotation = Quaternion.LookRotation(Vector3.forward, perpendicular);
	}
}
