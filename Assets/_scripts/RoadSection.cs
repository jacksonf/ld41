﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JacksonFrankland.Engine.Variables;

public class RoadSection : MonoBehaviour
{
	public BoolReference isBottom;
	public BoolReference isTop;
	public IntReference score;

	private static Transform[] sections = new Transform[3];
	private static Transform[] startingSections = new Transform[3];
	private Vector3 startingPosition;

	private float sectitonOffset = 12.4f;

	public static void Reset ()
	{
		sections[0] = startingSections[0];
		sections[1] = startingSections[1];
		sections[2] = startingSections[2];
		sections[0].GetComponent<RoadSection>().isBottom.Value = true;
		sections[0].GetComponent<RoadSection>().isTop.Value = false;
		sections[1].GetComponent<RoadSection>().isBottom.Value = false;
		sections[1].GetComponent<RoadSection>().isTop.Value = false;
		sections[2].GetComponent<RoadSection>().isBottom.Value = false;
		sections[2].GetComponent<RoadSection>().isTop.Value = true;
		foreach (Transform section in sections)
		{
			section.position = section.GetComponent<RoadSection>().startingPosition;
		}
	}

	void Start()
	{
		if (isBottom) {
			sections[0] = transform;
			startingSections[0] = transform;
		} else if (isTop) {
			sections[2] = transform;
			startingSections[2] = transform;
		} else {
			sections[1] = transform;
			startingSections[1] = transform;
		}
		startingPosition = transform.position;
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if (!other.CompareTag("Player")) {
			return;
		}
		Debug.Log("player");
		if (isTop.Value) {
			sections[0].position = new Vector3(
				transform.position.x,
				transform.position.y + sectitonOffset,
				transform.position.z
			);
			Transform[] newSections = new Transform[3];
			newSections[0] = sections[1];
			newSections[1] = sections[2];
			newSections[2] = sections[0];
			sections[2].GetComponent<RoadSection>().isTop.Value = false;
			sections[0].GetComponent<RoadSection>().isBottom.Value = false;
			newSections[2].GetComponent<RoadSection>().isTop.Value = true;
			newSections[0].GetComponent<RoadSection>().isBottom.Value = true;
			sections = newSections;
		}
		else if(isBottom.Value && score > 1) {
			sections[2].position = new Vector3(
				transform.position.x,
				transform.position.y - sectitonOffset,
				transform.position.z
			);
			Transform[] newSections = new Transform[3];
			newSections[0] = sections[2];
			newSections[1] = sections[0];
			newSections[2] = sections[1];
			sections[2].GetComponent<RoadSection>().isTop.Value = false;
			sections[0].GetComponent<RoadSection>().isBottom.Value = false;
			newSections[2].GetComponent<RoadSection>().isTop.Value = true;
			newSections[0].GetComponent<RoadSection>().isBottom.Value = true;
			sections = newSections;
		}
	}

}
