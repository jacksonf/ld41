using System;
using UnityEngine;

namespace JacksonFrankland.Engine.Variables
{
    [Serializable]
    public class Vector2Reference : Reference<Vector2, Vector2Variable>
    {
    }
}