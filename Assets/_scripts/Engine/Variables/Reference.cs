// ----------------------------------------------------------------------------
using System;

namespace JacksonFrankland.Engine.Variables
{
    public class Reference<T, U> where U : Variable<T>
    {
        public bool UseConstant = true;
        public T ConstantValue;
        public U Variable;

        public Reference()
        { }

        public Reference(T value)
        {
            UseConstant = true;
            ConstantValue = value;
        }

        public T Value
        {
            get 
            {
                return UseConstant ? ConstantValue : Variable.Value;
            }
            set 
            {
                if (UseConstant) {
                    ConstantValue = value;
                } else {
                    Variable.Value = value;
                }
            }
        }

        public static implicit operator T(Reference<T, U> reference)
        {
            return reference.Value;
        }
    }
}