using System;
using UnityEngine;

namespace JacksonFrankland.Engine.Variables
{
    [Serializable]
    public class FloatReference : Reference<float, FloatVariable>
    {
    }
}