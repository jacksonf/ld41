using UnityEngine;

namespace JacksonFrankland.Engine.Variables
{
    [CreateAssetMenu]
    public class Vector2Variable : Variable<Vector2>
    {
    }
}