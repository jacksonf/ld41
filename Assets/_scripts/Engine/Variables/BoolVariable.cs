using UnityEngine;

namespace JacksonFrankland.Engine.Variables
{
    [CreateAssetMenu]
    public class BoolVariable : Variable<bool>
    {
    }
}