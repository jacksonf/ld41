using System;
using UnityEngine;

namespace JacksonFrankland.Engine.Variables
{
    [Serializable]
    public class IntReference : Reference<int, IntVariable>
    {
    }
}