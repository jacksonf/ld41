using UnityEngine;

namespace JacksonFrankland.Engine.Variables
{
    [CreateAssetMenu]
    public class FloatVariable : Variable<float>
    {
    }
}