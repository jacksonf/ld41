using UnityEngine;

namespace JacksonFrankland.Engine.Variables
{
    [CreateAssetMenu]
    public class IntVariable : Variable<int>
    {
    }
}