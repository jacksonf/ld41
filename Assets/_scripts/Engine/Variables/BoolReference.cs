using System;
using UnityEngine;

namespace JacksonFrankland.Engine.Variables
{
    [Serializable]
    public class BoolReference : Reference<bool, BoolVariable>
    {
    }
}