﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JacksonFrankland.Engine.Variables;

public class FlyGenerator : MonoBehaviour {

	public IntReference score;
	public GameObject flyPrefab;
	public FloatReference minIntervalTime;
	public FloatReference maxIntervalTime;
	public float positionVariation;

	private float intervalTime;
	private float lastSpawnTime;

	void Start ()
	{
		lastSpawnTime = Time.time;
		intervalTime = 0;
	}
	
	void Update () 
	{
		if(Time.time - lastSpawnTime > intervalTime && score.Value > 0) {
			GameObject fly = Instantiate(flyPrefab);
			fly.transform.position = transform.position + Vector3.forward * Random.Range(positionVariation * -1, positionVariation);
			lastSpawnTime = Time.time;
			intervalTime = Random.Range(minIntervalTime, maxIntervalTime);
		}
	}
}
