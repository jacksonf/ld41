﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JacksonFrankland.Engine.Variables;

public class Frog : MonoBehaviour {

	public FloatReference shootRate;
	public FloatReference bulletSpeed;
	public GameObject bulletPrefab;
	public IntReference highScore;
	public IntReference previousScore;
	public IntReference score;

	private float lastShootTime;	
	private Vector3 spawnPosition;
	

	void Start ()
	{
		lastShootTime = Time.time;
		spawnPosition = transform.position;
	}
	
	void Update ()
	{
		if (Input.GetMouseButton(0) && Time.time - lastShootTime > shootRate) {
			GameObject bullet = Instantiate(bulletPrefab);
			bullet.transform.position = transform.position;
			bullet.GetComponent<Rigidbody2D>().velocity 
				= (Vector2)(Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).normalized * bulletSpeed;
			Destroy(bullet, 5f);
			lastShootTime = Time.time;
		}
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if(other.transform.GetComponent<Car>() == null) {
			return;
		}
		Fly.DestroyFlys();
		RoadSection.Reset();
		highScore.Value = score.Value > highScore.Value ? score.Value : highScore.Value;
		previousScore.Value = score.Value;
		transform.position = spawnPosition;
		Camera.main.transform.position = spawnPosition + (Vector3.forward * Camera.main.transform.position.z);
		GetComponent<AudioSource>().Play();
	}
	private void OnCollisionStay2D(Collision2D other)
	{
		if(!other.transform.CompareTag("Car")) {
			return;
		}
		Fly.DestroyFlys();
		RoadSection.Reset();
		highScore.Value = score.Value > highScore.Value ? score.Value : highScore.Value;
		previousScore.Value = score.Value;
		transform.position = spawnPosition;
		Camera.main.transform.position = spawnPosition + (Vector3.forward * Camera.main.transform.position.z);
		GetComponent<AudioSource>().Play();
	}
}
