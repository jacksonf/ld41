﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JacksonFrankland.Engine.Variables;

[RequireComponent(typeof(UpdateVelocity))]
public class Follow : MonoBehaviour {

	public Vector2Reference followPoint;

	private UpdateVelocity updateVelocity;

	void Start ()
	{
		updateVelocity = GetComponent<UpdateVelocity>();
	}

	void Update ()
	{
		Vector2 direction = followPoint.Value - (Vector2)transform.position;
		updateVelocity.direction = direction;
	}
}
