using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JacksonFrankland.Engine.Variables;

[RequireComponent(typeof(Rigidbody2D))]
public class UpdateVelocity : MonoBehaviour
{
	public FloatReference speed;
	public FloatReference speedSmoothTime;
	public BoolReference isAnimated;
	public string toAnimatorParameter;

    [HideInInspector] public Vector2 direction;

	private Vector2 velocity = Vector2.zero;
	private Rigidbody2D rb;
	private Animator animator;

	void Start ()
	{
		rb = GetComponent<Rigidbody2D>();
		animator = GetComponent<Animator>();
	}
	
	void Update ()
	{
		Vector2 xVelocity = direction.x != 0 ? Vector2.right * Mathf.Sign(direction.x) * speed : Vector2.zero;
		Vector2 yVelocity = direction.y != 0 ? Vector2.up * Mathf.Sign(direction.y) * speed : Vector2.zero;
        Vector2 targetVelocity = xVelocity + yVelocity;
		rb.velocity = Vector2.SmoothDamp(rb.velocity, targetVelocity, ref velocity, speedSmoothTime, Mathf.Infinity, Time.deltaTime);
		if (isAnimated && animator) {
			animator.SetFloat(toAnimatorParameter, rb.velocity.magnitude);
		}
	}

}